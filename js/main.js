var PI_CONNECTOR_MODEL = 'Connector - OSI PI';
var PI_ETL_MODEL = 'ETL - OSI PI';
var PI_ETL_STUDY = 'Study';
var PI_ETL_SCENARIO = 'Scenario';
var TAGS = [];
var ASSETS = {};
window.GRID_API = null;

function addRow() {
    window.GRID_API.api.updateRowData({add:[{}]});
}

function deleteRow() {
    var selectedRows = window.GRID_API.api.getSelectedRows();
    
    if (selectedRows.length > 0) {
        window.GRID_API.api.updateRowData({remove:selectedRows});    
    }
}

async function forceExecute() {
    await fetch(getInstallLocation() + 'api/v1/execute/' + PI_ETL_MODEL + '/' + PI_ETL_STUDY, {
        method: 'post',
        headers: {
            "Authorization": apiToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify([PI_ETL_SCENARIO])
    })
    .catch(function(error) {
        NotifyError('An error occurred making an Akumen Fetch API call: ' + error);
    });
    
    NotifyPopupDefault("Executing ETL");
}

async function saveData() {
    window.GRID_API.api.stopEditing();
    
    var data = {'data': []}
    window.GRID_API.api.forEachNode(function(node, index) {
        data['data'].push(node.data);
    });
    
    var data = await fetch(getInstallLocation() + 'api/v1/models/' + PI_ETL_MODEL + '/' + PI_ETL_STUDY + '/' + PI_ETL_SCENARIO + '/SaveInputParameter', {
        method: 'post',
        headers: {
            "Authorization": apiToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            'input_parameter': {
                'parameter_name': 'data',
                'extension_data': JSON.stringify(data)
            },
            'scope': 'Model',
            'reset_run_status': false
        })
    })
    .then(function() {
        NotifyPopupSuccess("Saved ETL configuration.");    
    })
    .catch(function(error) {
        NotifyError('An error occurred making an Akumen Fetch API call: ' + error);
    });
}

function initialiseGrid(id, assets, tags) {
    var columnDefs = [
        {headerName: "Asset", field: "asset", sortable: true, editable: true, cellEditor: 'agSelectCellEditor',
            cellEditorParams: {values: Object.keys(assets)},
        checkboxSelection: true
        },
        {headerName: "Attribute", field: "attribute", sortable: true, editable: true, cellEditor: 'agSelectCellEditor',
            cellEditorParams: function(params) {
                return {values: Object.keys(assets[params.data.asset].attributes)};
            }
        },
        {headerName: "Tag", field: "tag", sortable: true, editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: {values:tags}},
        {headerName: "Sample Method", field: "samplemethod", sortable: true, editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: {values:["Average", "Snapshot", "Raw"]}},
        {headerName: "Sample Period", field: "sampleperiod", sortable: true, editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: {values:["1m", "5m", "30m", "60m"]}}
    ];
    
    window.GRID_API = {
        columnDefs: columnDefs
    };
    
    var gridDiv = document.querySelector(id);
    new agGrid.Grid(gridDiv, GRID_API);
    
    window.GRID_API.api.sizeColumnsToFit();
}

async function getAssets() {
    var assets = {};
    var page = 0;
    var pageSize = 20;
    
    while (true) {
        var result = await fetch(getInstallLocation() + 'api/v2/assets?page=' + page + '&pageSize=' + pageSize, {
            method: 'get',
            headers: {
                "Authorization": apiToken,
                'Content-Type': 'application/json'
            }
        })
        .then(function (res) {return res.json(); })
        .catch(function(error) {
            NotifyError('An error occurred making an Akumen Fetch API call: ' + error);
        });    
        
        assets = Object.assign({}, assets, result);
        
        if (Object.keys(result).length < pageSize) {
            break;
        }
        
        page++;
    }
    
    
    return assets;
}

async function getTags() {
    var result = await fetch(getInstallLocation() + 'api/v1/execute/' + PI_CONNECTOR_MODEL, {
        method: 'post',
        headers: {
            "Authorization": apiToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({'method': 'get_tag_list', 'params': '{}', 'simulate': true})
        
    })
    .then(function (res) {
        return res.json();
    })
    .catch(function(error) {
        NotifyError('An error occurred making an Akumen Fetch API call: ' + error);
    });
        
    if (result['status'] === 'Error') {
        console.error(result['error_log']);
    }
    
    outputs = {};
    for (var key in result['simple_outputs']) {
        if (key === "InternalOutput") {
            var item = result['simple_outputs'][key];
            for (var output of item) {
                outputs[output.Name] = output.StringValue;
            }
        }
    }

    return JSON.parse(outputs['result'])['tags'];
}

async function getData() {
    var data = await fetch(getInstallLocation() + 'api/v1/models/' + PI_ETL_MODEL + '/' + PI_ETL_STUDY + '/' + PI_ETL_SCENARIO + '/input_parameters', {
        method: 'get',
        headers: {
            "Authorization": apiToken,
            'Content-Type': 'application/json'
        }
    })
    .then(function (res) {
        return res.json();
    })
    .catch(function(error) {
        NotifyError('An error occurred making an Akumen Fetch API call: ' + error);
    });

    inputs = null;
    for (var parameter of data) {
        if (parameter.parameter_name === 'data') {
            if (parameter.extension_data !== null) {
                inputs = JSON.parse(parameter.extension_data);    
            } else {
                inputs = {'data':[]}
            }
            
            break;
        }
    }
    
    return inputs['data'];
}

function resize() {
    if (window.GRID_API.api !== undefined) {
        window.GRID_API.api.sizeColumnsToFit();    
    }
}

$(document).ready(async function() {
    $(window).resize(resize);
    
    var tags = await getTags();
    var assets = await getAssets();
    var data = await getData();
    
    initialiseGrid('#etl-grid', assets, tags);
    window.GRID_API.api.setRowData(data);
    $('#gridLoader').removeClass('active');
});